package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            String RPNExpression = getRPNExpression(statement);
            String result = getResult(RPNExpression);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    private String getRPNExpression(String inputExpression) {
        String RPNExpression = "";
        Stack<Character> operations = new Stack<>();

        for (int i = 0; i < inputExpression.length(); i++) {

            if (Character.isDigit(inputExpression.charAt(i))) {
                while (!isOperand(inputExpression.charAt(i))) {
                    RPNExpression += inputExpression.charAt(i);
                    i++;

                    if (i == inputExpression.length()) {
                        break;
                    }
                }

                i--;
                RPNExpression += " ";
            }

            if (isOperand(inputExpression.charAt(i))) {

                if (inputExpression.charAt(i) == '(') {
                    operations.push(inputExpression.charAt(i));
                } else if (inputExpression.charAt(i) == ')') {
                    Character s = operations.pop();

                    while (s != '(') {
                        RPNExpression += s.toString() + " ";
                        s = operations.pop();
                    }
                } else {
                    if (!operations.empty()) {
                        if ((getPriority(inputExpression.charAt(i)) <= getPriority(operations.peek()))) {

                            RPNExpression += operations.pop().toString() + " ";
                        }
                    }
                    operations.push(inputExpression.charAt(i));
                }
            }
        }


        while (!operations.empty()) {
            RPNExpression += operations.pop().toString() + " ";
        }

        return RPNExpression;
    }


    private byte getPriority(Character s) {
        switch (s) {
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
                return 2;
            case '-':
                return 3;
            case '*':
                return 4;
            case '/':
                return 5;
            default:
                return 6;
        }
    }

    private String getResult(String RPNExpression) {
        String result = "";
        double resultDouble = 0;
        Stack<Double> numbers = new Stack<>();

        for (int i = 0; i < RPNExpression.length(); i++) {

            if (Character.isDigit(RPNExpression.charAt(i))) {
                String number = "";

                while ((!isOperand(RPNExpression.charAt(i))) && (!isDelimeter(RPNExpression.charAt(i)))) {
                    number += RPNExpression.charAt(i);
                    i++;

                    if (i == RPNExpression.length()) {
                        break;
                    }
                }

                numbers.push(Double.parseDouble(number));
                i--;
            } else if (isOperand(RPNExpression.charAt(i))) {
                double firstNumber = numbers.pop();
                double secondNumber = numbers.pop();

                switch (RPNExpression.charAt(i)) {
                    case '+':
                        resultDouble = firstNumber + secondNumber;
                        break;
                    case '-':
                        resultDouble = secondNumber - firstNumber;
                        break;
                    case '/':
                        if(firstNumber == 0){
                            return null;
                        }
                        resultDouble = secondNumber / firstNumber;
                        break;
                    case '*':
                        resultDouble = firstNumber * secondNumber;
                        break;
                }
                numbers.push(resultDouble);
            }

        }

        String a = numbers.pop().toString();
        int b = a.indexOf(".");

        if(a.length() - b - 1 > 1 ){
            return a;
        } else {
           Integer resultD = (int)Double.parseDouble(a);
           return resultD.toString();
        }

    }


    private boolean isDelimeter(Character s) {
        return s.equals(' ');
    }

    private boolean isOperand(Character s) {
        switch (s) {
            case '(':
                return true;
            case ')':
                return true;
            case '+':
                return true;
            case '-':
                return true;
            case '*':
                return true;
            case '/':
                return true;
            default:
                return false;
        }
    }


}
