package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException{

        if ((x == null)||(y == null)){
            throw new IllegalArgumentException();
        }

        boolean YContainsX = y.containsAll(x);
        int count = 0;
        boolean find = false;

        if(x.size() == 0){
            find = true;
        }

        if (YContainsX) {
            for (int i = 0; i < x.size() - 1; i++) {
                int index = y.indexOf(x.get(i));
                int index1 = y.indexOf(x.get(i + 1));
                if (index1 > index) {
                    count++;
                }
            }
        } else {
            find = false;
        }

        if (count == x.size() - 1) {
            find = true;
        }

        return find;
    }
}
