package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        boolean isCanBuildPyramid = false;
        int size = inputNumbers.size();
        int count = 0;
        int row = 1;
        int col = 1;
        int pyramid[][];

        while (count < size) {
            count += row;
            row++;
            col += 2;
        }

        row -= 1;
        col -= 2;

        if (size == count) {
            isCanBuildPyramid = true;
        }

        try {
            if (isCanBuildPyramid) {
                List<Integer> sortedList = inputNumbers.stream().sorted().collect(Collectors.toList());
                pyramid = new int[row][col];

                int center = (col / 2);
                count = 1;
                int arrIdx = 0;

                for (int i = 0, offset = 0; i < row; i++, offset++, count++) {
                    int start = center - offset;
                    for (int j = 0; j < count * 2; j += 2, arrIdx++) {
                        pyramid[i][start + j] = sortedList.get(arrIdx);
                    }
                }


            } else {
                throw new CannotBuildPyramidException();
            }
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        } catch (IllegalArgumentException e) {
            throw new CannotBuildPyramidException();

        }


        return pyramid;
    }
}